class Solution
  ONE = 1
  attr_accessor :chairs

  def initialize(n: 100)
    @chairs = []
    (1..n).each do |i|
      chairs.push(i)
    end
  end

  def result
    index = 0; skip = 0;
    while(chairs.length > ONE)
      gone = chairs.slice!(index, ONE)
      #log_info "gone = #{gone[0]} & chairs_length = #{chairs.length}"
      skip += ONE
      index += skip
      #log_info "skip_value=#{skip} & index_1=#{index}"
      index %= chairs.length
      #log_info "skip_value=#{skip} & index_2=#{index}"
    end
    return chairs[0];
  end

  private

  def log_info message
    puts message
  end
end