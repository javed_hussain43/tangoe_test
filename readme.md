# Instructions to run application

1) cd tangoe_test/

2) Application requires ruby. Before you begin; install the dependencies by running `bundle`.
Once the dependencies have been installed you'll have a few commands available:

- `./solution n`  : Will attempt to run your application and print output to the terminal.
- `./solution` : Runs the solution for default 100 chairs.

* The value of `n` should be greater or equal to 1. 